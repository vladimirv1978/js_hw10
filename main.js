// -----------------С использованием делегирования событий------------------------

let tabsContent = [...document.querySelectorAll(".tabs-content>li")];
let tabs = [...document.querySelectorAll(".tabs>li")];

function activeElementDisplay(activeContent = alert("Вы выбрали несколько вкладок одновременно, выберите нужную вкладку"),activeTab) {
  tabsContent.forEach((elem) => (elem.hidden = true));
  tabs.forEach((elem) => elem.classList.remove("active"));

  activeTab.classList.add("active");
  activeContent.hidden = false;
}

function setListnerButton() {
  document.querySelector(".tabs").addEventListener("click", function (event) {
    let [curentContent] = tabsContent.filter(
      (elem) => elem.dataset.name == event.target.textContent
    );
    activeElementDisplay(curentContent, event.target);
  });
}

activeElementDisplay(tabsContent[0], tabs[0]);
setListnerButton();

// -----------------Без использования делегирования событий------------------------
// let tabsContent = document.querySelectorAll(".tabs-content>li");
// let tabs = document.querySelectorAll(".tabs>li");

// function activeElementDisplay(activeElem) {
//   console.log(activeElem);
//   tabsContent.forEach((elem) => (elem.hidden = true));
//   tabs.forEach((elem) => elem.classList.remove("active"));
//   tabs[activeElem].classList.add("active");
//   tabsContent[activeElem].hidden = false;
// };

// function setListnerButton() {
//   for (let i = 0; i <= tabs.length - 1; i++) {
//     tabs[i].addEventListener("click", function () {
//       console.log(i);
//       activeElementDisplay(i);

//       if (!this.dataset.name) {
//         this.dataset.name = this.textContent;
//         tabsContent[i].dataset.name = this.textContent;
//         tabs[i].classList.add("active");
//       };
//     });
//   };
// };

// activeElementDisplay(0);
// setListnerButton();
